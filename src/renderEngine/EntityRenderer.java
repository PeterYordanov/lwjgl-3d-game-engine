package renderEngine;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import entities.Entity;
import models.Model;
import models.TexturedModel;
import shaders.StaticShader;
import terrains.Terrain;
import textures.ModelTexture;
import java.util.ArrayList;
import java.util.Map;

public class EntityRenderer {
	
	private StaticShader shader;
	
	public EntityRenderer(StaticShader shader, Matrix4f projectionMatrix) 
	{
		this.shader = shader;
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	
	public void render(Map<TexturedModel, ArrayList<Entity>> entities) 
	{
		for(TexturedModel model : entities.keySet()) {
			prepareTexturedModel(model);
			ArrayList<Entity> batch = entities.get(model);
			for(Entity entity : batch) {
				prepareInstance(entity);
				GL11.glDrawElements(GL11.GL_TRIANGLES, model.model().vertexCount(), GL11.GL_UNSIGNED_INT, 0);
			}
			
			unbindTexturedModel();
		}
	}
	
	public void prepareTexturedModel(TexturedModel texturedModel) 
	{
		Model model = texturedModel.model();
		GL30.glBindVertexArray(model.vertexArrayObjectID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		ModelTexture texture = texturedModel.texture();
		if(texture.hasTransparency())
			MasterRenderer.disableCulling();
	
		shader.loadFakeLightingVariable(texture.useFakeLighting());
		shader.loadShineVariables(texture.shineDamper(), texture.reflectivity());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.id());
		
	}

	public void unbindTexturedModel() 
	{
		MasterRenderer.enableCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}
	
	public void prepareInstance(Entity entity) 
	{
		Matrix4f transformationMatrix = math.Helpers.createTransformationMatrix(entity.position(), entity.rotX(), entity.rotY(), entity.rotZ(), entity.scale());
		shader.loadTransformationMatrix(transformationMatrix); 
	}

	public void clear() 
	{
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(0, 0, 0, 1);
	}
}
