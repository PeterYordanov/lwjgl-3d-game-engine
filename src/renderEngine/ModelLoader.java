package renderEngine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import models.Model;

public class ModelLoader {
	
	private ArrayList<Integer> VAOs = new ArrayList<Integer>();
	private ArrayList<Integer> VBOs = new ArrayList<Integer>();
	private ArrayList<Integer> textures = new ArrayList<Integer>();
	
	public Model loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices) 
	{
		//Create empty Vertex Array Object
		int vaoID = GL30.glGenVertexArrays();
		VAOs.add(vaoID);
		GL30.glBindVertexArray(vaoID);
		
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, positions, 3);
		storeDataInAttributeList(1, textureCoords, 2);
		storeDataInAttributeList(2, normals, 3);
		
		//Unbind VAO
		GL30.glBindVertexArray(0);
		
		return new Model(vaoID, indices.length);
	}
	
	public int loadTexture(String fileName) 
	{
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("src/res/" + fileName + ".png"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textures.add(textureID);
		return textureID;
	}
	
	public void clear() 
	{
		for(int vaoID : VAOs)
			GL30.glDeleteVertexArrays(vaoID);
		
		for(int vboID : VBOs)
			GL15.glDeleteBuffers(vboID);
		
		for(int texture : textures)
			GL11.glDeleteTextures(texture);
		
		VAOs.clear();
		VBOs.clear();
	}

	//Of VAO
	private void storeDataInAttributeList(int attributeNumber, float[] vertices, int coordSize) 
	{
		int vboID = GL15.glGenBuffers();
		VBOs.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(vertices);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber,	/* Attribute list index number */
								   coordSize, 		/* Length of vertex (3D vectors) */
				   				   GL11.GL_FLOAT, 	/* Type of data */ 
				   				   false, 		  	/* Is data normalized */
				   				   0, 			  	/* Distance between vertices */  
				   				   0 				/* Offset, start at the beginning of the data */);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
	}
	
	private void bindIndicesBuffer(int[] indices) 
	{
		int vboID = GL15.glGenBuffers();
		VBOs.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	private IntBuffer storeDataInIntBuffer(int[] data) 
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	private FloatBuffer storeDataInFloatBuffer(float[] data) 
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
}
