package renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class Window {
	
	private static int width = 0;
	private static int height = 0;
	private static int fps = 0;
	
	public static void open(int w, int h, String title, int fps) 
	{
		if(w > 100 && h > 100) {
			width = w;
			height = h;
		} else {
			width = 100;
			height = 100;
		}
		
		if(fps < 30)
			fps = 30;
		
		ContextAttribs attribs = new ContextAttribs(3, 2);
		attribs.withForwardCompatible(true);
		attribs.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle(title);
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GL11.glViewport(0, 0, width, height);
	}
	
	public static void update() 
	{
		Display.sync(fps);
		Display.update();
	}
	
	public static void close() 
	{
		Display.destroy();
	}
	
	public static boolean isCloseRequested() 
	{
		return Display.isCloseRequested();
	}
}
