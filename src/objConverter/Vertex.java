package objConverter;

import org.lwjgl.util.vector.Vector3f;

public class Vertex {
     
    private static final int NO_INDEX = -1;
     
    private Vector3f position;
    private int textureIndex = NO_INDEX;
    private int normalIndex = NO_INDEX;
    private Vertex duplicateVertex = null;
    private int index;
    private float length;
     
    public Vertex(int index,Vector3f position)
    {
        this.index = index;
        this.position = position;
        this.length = position.length();
    }
     
    public int index()
    {
        return index;
    }
     
    public float length(){
        return length;
    }
     
    public boolean isSet(){
        return textureIndex!=NO_INDEX && normalIndex!=NO_INDEX;
    }
     
    public boolean hasSameTextureAndNormal(int textureIndexOther,int normalIndexOther){
        return textureIndexOther==textureIndex && normalIndexOther==normalIndex;
    }
     
    public void setTextureIndex(int textureIndex){
        this.textureIndex = textureIndex;
    }
     
    public void setNormalIndex(int normalIndex){
        this.normalIndex = normalIndex;
    }
 
    public Vector3f position() 
    {
        return position;
    }
 
    public int textureIndex() 
    {
        return textureIndex;
    }
 
    public int normalIndex() 
    {
        return normalIndex;
    }
 
    public Vertex duplicateVertex() 
    {
        return duplicateVertex;
    }
 
    public void setDuplicateVertex(Vertex duplicateVertex) 
    {
        this.duplicateVertex = duplicateVertex;
    }
}