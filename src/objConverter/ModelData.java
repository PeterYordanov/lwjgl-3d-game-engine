package objConverter;

public class ModelData {
	 
    private float[] vertices;
    private float[] textureCoords;
    private float[] normals;
    private int[] indices;
    private float furthestPoint;
 
    public ModelData(float[] vertices, float[] textureCoords, float[] normals, int[] indices,
            float furthestPoint) {
        this.vertices = vertices;
        this.textureCoords = textureCoords;
        this.normals = normals;
        this.indices = indices;
        this.furthestPoint = furthestPoint;
    }
 
    public float[] vertices() {
        return vertices;
    }
 
    public float[] textureCoords() {
        return textureCoords;
    }
 
    public float[] normals() {
        return normals;
    }
 
    public int[] indices() {
        return indices;
    }
 
    public float furthestPoint() {
        return furthestPoint;
    }
 
}