package shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Light;

public class StaticShader extends AbstractShader {

	private static final String VERTEX_FILE = "src/shaders/vertexShader.txt";
	private static final String FRAGMENT_FILE = "src/shaders/fragmentShader.txt"; 
	private int locationTransformationMatrix;
	private int locationProjectionMatrix;
	private int locationViewMatrix;
	private int locationLightPosition;
	private int locationLightColor;
	private int locationShineDamper;
	private int locationReflectivity;
	private int locationUseFakeLighting;
	private int locationSkyColor;

	public StaticShader() 
	{
		super(VERTEX_FILE, FRAGMENT_FILE);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void bindAttributes() 
	{
		super.bindAttributes(0, "position");
		super.bindAttributes(1, "textureCoords");
		super.bindAttributes(2, "normal");
	}

	@Override
	protected void allUniformLocations() {
		locationTransformationMatrix = super.uniformLocation("transformationMatrix");
		locationProjectionMatrix = super.uniformLocation("projectionMatrix");
		locationViewMatrix = super.uniformLocation("viewMatrix");
		locationLightPosition = super.uniformLocation("lightPosition");
		locationLightColor = super.uniformLocation("lightColor");
		locationShineDamper = super.uniformLocation("shineDamper");
		locationReflectivity = super.uniformLocation("reflectivity");
		locationUseFakeLighting = super.uniformLocation("useFakeLighting");
		locationSkyColor = super.uniformLocation("skyColor");
	}
	
	public void loadSkyColor(float r, float g, float b) 
	{
		super.loadVector(locationSkyColor, new Vector3f(r, g, b));
	}
	
	public void loadFakeLightingVariable(boolean useFake) 
	{
		super.loadBoolean(locationUseFakeLighting, useFake);
	}
	
	public void loadShineVariables(float damper, float reflectivity) 
	{
		super.loadFloat(locationShineDamper, damper);
		super.loadFloat(locationReflectivity, reflectivity);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix) 
	{
		super.loadMatrix(locationTransformationMatrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix) 
	{
		super.loadMatrix(locationProjectionMatrix, matrix);
	}
	
	public void loadViewMatrix(Camera cam) 
	{
		Matrix4f viewMatrix = math.Helpers.createViewMatrix(cam);
		super.loadMatrix(locationViewMatrix, viewMatrix);
	}
	
	public void loadLight(Light light) 
	{
		super.loadVector(locationLightPosition, light.position());
		super.loadVector(locationLightColor, light.color());
	}
}
