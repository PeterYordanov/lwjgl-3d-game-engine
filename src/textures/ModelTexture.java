package textures;

public class ModelTexture {
	
	private int textureID;
	private float shineDamper = 1;
	private float reflectivity = 0;
	private boolean hasTransparency = false;
	private boolean useFakeLighting = false;
	
	public ModelTexture(int id) 
	{
		textureID = id;
	}
	

	
	public boolean useFakeLighting() 
	{
		return useFakeLighting;
	}
	
	public void setUseFakeLighting(boolean use) 
	{
		useFakeLighting = use;
	} 
	
	
	public boolean hasTransparency() 
	{
		return hasTransparency;
	}
	
	public void setTransparency(boolean has) 
	{
		hasTransparency = has;
	} 
	
	public int id() 
	{
		return textureID;
	}
	
	public float shineDamper() {
		return shineDamper;
	}

	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}

	public float reflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
}
