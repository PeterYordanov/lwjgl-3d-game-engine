package textures;

public class TerrainTexturePack {
	private TerrainTexture backgroundTexture;
	private TerrainTexture rTexture;
	private TerrainTexture gTexture;
	private TerrainTexture bTexture;
	
	public TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture, TerrainTexture gTexture, TerrainTexture bTexture) 
	{
		this.backgroundTexture = backgroundTexture;
		this.rTexture = rTexture;
		this.gTexture = gTexture;
		this.bTexture = bTexture;
	}

	public TerrainTexture backgroundTexture() {
		return backgroundTexture;
	}

	public void setBackgroundTexture(TerrainTexture backgroundTexture) {
		this.backgroundTexture = backgroundTexture;
	}

	public TerrainTexture rTexture() {
		return rTexture;
	}

	public void setRTexture(TerrainTexture rTexture) {
		this.rTexture = rTexture;
	}

	public TerrainTexture gTexture() {
		return gTexture;
	}

	public void setGTexture(TerrainTexture gTexture) {
		this.gTexture = gTexture;
	}

	public TerrainTexture bTexture() {
		return bTexture;
	}

	public void setBTexture(TerrainTexture bTexture) {
		this.bTexture = bTexture;
	}
	
}
