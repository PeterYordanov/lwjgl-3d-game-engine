package models;

public class Model {
	private int vertexArrayObjectID;
	private int vertexCount;
	
	public Model(int vaoID, int vertexCount) 
	{
		vertexArrayObjectID = vaoID;
		this.vertexCount = vertexCount;
	}
	
	public int vertexArrayObjectID() 
	{
		return vertexArrayObjectID;
	}
	
	public int vertexCount() 
	{
		return vertexCount;
	}
}
