package models;

import textures.ModelTexture;

public class TexturedModel {
	private Model rawModel;
	private ModelTexture texture;
	
	public TexturedModel(Model model, ModelTexture texture) 
	{
		rawModel = model;
		this.texture = texture;
	}
	
	public Model model() 
	{
		return rawModel;
	}
	
	public ModelTexture texture() 
	{
		return texture;
	}
}
