package engine;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.Model;
import models.TexturedModel;
import objConverter.ModelData;
import objConverter.OBJFileLoader;
import renderEngine.MasterRenderer;
import renderEngine.ModelLoader;
import renderEngine.OBJLoader;
import renderEngine.Window;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;

public class GameLoop {
	
	public static void main(String[] args) 
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		Window.open(screenSize.width / 2, screenSize.height / 2, "3D Game Engine", 60);
		ModelLoader loader = new ModelLoader();
		
		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("mud"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("pathway"));
		
		TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));
		
		Terrain terrain = new Terrain(0, 0, loader, texturePack, blendMap);
		Terrain terrain2 = new Terrain(1, 0, loader, texturePack, blendMap);
		
		Camera camera = new Camera();
		Light light = new Light(new Vector3f(0, 150, -20), new Vector3f(1, 1, 1));
		
		MasterRenderer renderer = new MasterRenderer();
		ArrayList<Entity> entities = new ArrayList<Entity>();
		Random rand = new Random();
		
		ModelData model = OBJFileLoader.loadOBJ("stall");
		Model rawModel = loader.loadToVAO(model.vertices(), model.textureCoords(), model.normals(), model.indices());
		
		for(int i = 0; i <= 10; i++) {
			Entity ent = new Entity(new TexturedModel(rawModel, new ModelTexture(loader.loadTexture("stallTexture"))), new Vector3f(i * 20, 0, -50), 0, 0, 0, 1);
			entities.add(ent);
		}
		
		for(int i = 0; i <= 200; i++) {
			Entity ent = new Entity(new TexturedModel(OBJLoader.loadObjModel("grassModel", loader), new ModelTexture(loader.loadTexture("grassTexture"))), new Vector3f(rand.nextInt(1500), 0, rand.nextInt(1500)), 0, 0, 0, 1);
			entities.add(ent);
		}
		
		for(int i = 0; i <= 200; i++) {
			Entity ent = new Entity(new TexturedModel(OBJLoader.loadObjModel("fern", loader), new ModelTexture(loader.loadTexture("fern"))), new Vector3f(rand.nextInt(1500), 0, rand.nextInt(1500)), 0, 0, 0, 1);
			ent.model().texture().setTransparency(true);
			ent.model().texture().setUseFakeLighting(true);
			entities.add(ent);
		}
		
		while(!Window.isCloseRequested()) {
			
			for(int i = 0; i <= entities.size(); i++) {
				if(i <= 10)
					entities.get(i).increaseRotation(0, 0.5f, 0);
			}
			
			for(Entity ent : entities) {
				renderer.processEntity(ent);
			}
			
			renderer.processTerain(terrain);
			renderer.processTerain(terrain2);
			renderer.render(light, camera);
			camera.move();
			Window.update();
		}
		
		renderer.destroy();
		loader.clear();
		Window.close();
	}
	
}
